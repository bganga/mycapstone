import { Injectable } from '@angular/core';
import { Message, MESSAGES } from './message/message';
import { Http, Headers } from '@angular/http';


@Injectable()
export class MessageService {

  constructor(private http: Http) { }

  private username:string;
  private circle: string;

  private BASE_URL = 'http://localhost:8087/api/message';
  // private GET_MSG_BY_USER='/getMessagesByUser/{senderUsername}/{receiverUserName}/{pageNumber}';
  // private GET_MSG_BY_CIRCLE='/getMessagesByCircle/{circleName}/{pageNumber}';


  getMessagesFromUser(userName: any) {
    this.username=userName;
    let cpHeaders = new Headers({ 'Content-Type':'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
  
    console.log('In service', userName);
    let loggedInUser = localStorage.getItem('username');
    return this.http.get(this.BASE_URL + '/getMessagesByUser/' + loggedInUser + '/' + userName + '/' + 0, { headers: cpHeaders });
  }

  sendMessageToUser(message: Message) {
    let cpHeaders = new Headers({ 'Content-Type':'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
  
    return this.http.post(this.BASE_URL + '/sendMessageToUser/' + message.recieverName,message, { headers: cpHeaders});
  }






  getMessagesFromCircle(circleName) {
    this.circle=circleName;
    let cpHeaders = new Headers({ 'Content-Type':'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
  
    console.log('In service', circleName);
    return this.http.get(this.BASE_URL + '/getMessagesByCircle/'+ circleName + '/0', { headers: cpHeaders });
  }

  sendMessageToCircle(message: any) {
    console.log('which circle',this.circle);
    let cpHeaders = new Headers({ 'Content-Type':'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
    return this.http.post(this.BASE_URL + '/sendMessageToCircle/' +this.circle ,message, { headers: cpHeaders});
  }
}
