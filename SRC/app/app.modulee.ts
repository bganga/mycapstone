import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { CircleComponent } from './circle/circle.component';
import { MessageComponent } from './message/message.component';
import { UserService } from './user.service';
import { CircleService } from './circle.service';
import { MessageService } from './message.service';
import { HttpModule} from '@angular/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { Component } from '@angular/core/src/metadata/directives';
import { FormsModule }   from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule,MatSelectModule,MatCardModule,MatButtonModule,MatToolbarModule,MatIconModule,MatListModule,MatTooltipModule,MatDialogModule,MatFormFieldModule} from '@angular/material';
import { UserCirclesComponent } from './user-circles/user-circles.component';
import { AddMembersComponent } from './add-members/add-members.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    CircleComponent,
    MessageComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    UserCirclesComponent,
    AddMembersComponent
    
  ],
  imports: [
    RouterModule.forRoot([
      {path : '', component: LoginComponent},
      {path : 'register', component: RegisterComponent},
      {path : 'dashboard', component: (localStorage.getItem('token') === null) ? LoginComponent : DashboardComponent}
    ]),
    BrowserModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  entryComponents: [UserCirclesComponent,AddMembersComponent],
  providers: [UserService,CircleService,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
