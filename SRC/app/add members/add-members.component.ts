import { Component, OnInit } from '@angular/core';
import { User } from '../user/user';
import { UserService } from '../user.service';
import { NgForm } from '@angular/forms';
import { CircleService } from '../circle.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-members',
  templateUrl: './add-members.component.html',
  styleUrls: ['./add-members.component.css']
})
export class AddMembersComponent implements OnInit {
  users: User[];
  admin = localStorage.getItem('username');

  constructor(private userService: UserService, private circleService: CircleService, public dialogRef: MatDialogRef<AddMembersComponent>) { }
  getUsers() {

    this.userService.getUsers().subscribe(data => {
      this.users = data.json();
    })
  }

  addMember(value) {
    if (value == this.admin) {
      alert("Self add");

    }
    else {
      this.circleService.addMember(value).subscribe(
        data => {
          if (data.status === 200) {
            alert("member added!");
            this.closeDialog();
          }
        })
    }

  }
  closeDialog() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.getUsers();
  }

}
