import { Component } from '@angular/core';
import {User} from './user/user';
import { UserService } from './user.service';
import {Circle} from './circle/circle';
import { CircleService } from './circle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  

  users: User[]=[];
  circles: Circle[]=[];
  constructor(private userService: UserService,private circleService: CircleService) { }
 
 
 getUsers() {
   this.userService.getUsers().subscribe(data =>{
     this.users=data.json();
   })
 }

 getCircles() {
  this.circleService.getCircles().subscribe(data =>{
    this.circles=data.json();
  })
}


 ngOnInit(){
   this.getUsers();
   this.getCircles();
  
 }  
}

