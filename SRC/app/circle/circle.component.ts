import { Component, OnInit, Input, Output,Inject } from '@angular/core';
import {Circle} from './circle';
import { CircleService } from '../circle.service';
import { EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { UserCirclesComponent } from '../user-circles/user-circles.component';


@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css'],
})
export class CircleComponent implements OnInit {
 
circle:string;

  circles: Circle[];

  @Output() selectedCircle= new EventEmitter<any>();

 
  constructor(private circleService: CircleService,public dialog: MatDialog) { }

  openDialog(): void {
    let dialogRef = this.dialog.open(UserCirclesComponent, {
      width: '350px',
    
     // data: { circleName: this.circle, creatorId: localStorage.getItem('username')}
    });
  
  }


  getCircle(){
    this.circleService.getCircles().subscribe(
      data=>{
      this.circles=data.json();
    }
  )
  }

  selectCircle(circleData: string){

    console.log('Circle Name:',circleData);
    localStorage.setItem('selectedCircle',circleData);
   
    let currentCircleName={
      type:'circle',
      value:circleData
    }
    console.log(currentCircleName);
    this.selectedCircle.emit(currentCircleName);
  }
  
 
  


  ngOnInit() {
 this.getCircle();
  }

}
