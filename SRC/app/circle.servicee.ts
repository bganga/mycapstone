import { Injectable } from '@angular/core';
//import {Circle, CIRCLES} from './circle/circle';
import { Http, Headers } from '@angular/http';
import { Circle } from './circle/circle';
import { HttpHeaders } from '@angular/common/http';
import { UserCircles } from './user-circles/user-circles';


@Injectable()
export class CircleService {

  private usercircle: UserCircles;

  constructor(private http: Http) { }
  private CIRCLE_SERVICE_BASE_URL = 'http://localhost:8084/api/circle';
  private ADD_USER_TO_CIRCLE = 'http://localhost:8085/api/usercircle/addToCircle/';

  headerObj = new Headers({
    'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  getCircles() {
    return this.http.get(this.CIRCLE_SERVICE_BASE_URL, { headers: this.headerObj });
  }

  createCircle(form) {
    console.log("creating", form.value.circlename);
    let obj = {
      'circleName': form.value.circlename,
      'creatorId': localStorage.getItem('username')
    }

    console.log(obj);
    let cpHeaders = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.post(this.CIRCLE_SERVICE_BASE_URL, obj, { headers: cpHeaders })
  }

  addAdmin(form) {
    let cpHeaders = new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token') });
    let circle = form.value.circlename;
    return this.http.put(this.ADD_USER_TO_CIRCLE + localStorage.getItem('username') + '/' + circle,{}, { headers: cpHeaders });
  }
  addMember(obj: any){
    let cpHeaders = new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token') });
    return this.http.put(this.ADD_USER_TO_CIRCLE +obj+ '/' + localStorage.getItem('selectedCircle'),{}, { headers: cpHeaders });
  }




}
