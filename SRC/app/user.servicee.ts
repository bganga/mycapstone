import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { User } from './user/user';
import { HttpHeaders } from '@angular/common/http';


@Injectable()
export class UserService {


  constructor(private http: Http) { }


  private USER_SERVICE_BASE_URL = 'http://localhost:8082';
  private GET_ALL_USERS = '/api/users';
  private USER_VAR='/user'
  private AUTHENTICATE = 'http://localhost:8082/login';

  headerObj = new Headers({
    'Authorization': 'Bearer ' + localStorage.getItem('token')
  });


  getUsers() {
    return this.http.get(this.USER_SERVICE_BASE_URL + this.GET_ALL_USERS, { headers: this.headerObj });
  }

  authenticateUser(form) {

    let cpHeaders = new Headers({'Content-Type':'application/json'});

    return this.http.post(this.AUTHENTICATE, form , { headers: cpHeaders });

  }

  registerUser(form) {

    let cpHeaders = new Headers({ 'Content-Type':'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
    return this.http.post(this.USER_SERVICE_BASE_URL + this.USER_VAR, form, { headers: cpHeaders } );
  }

}
