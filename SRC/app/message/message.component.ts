import { Component, OnInit, Input, EventEmitter,Inject} from '@angular/core';
import { Message } from './message';
import { MessageService } from '../message.service';
import { OnChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AddMembersComponent } from '../add-members/add-members.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})

export class MessageComponent implements OnInit,OnChanges {


  constructor(private messageService: MessageService,public dialog: MatDialog) { }

  loggedInUser=localStorage.getItem('username');
  messages = [];
  userdata: string;
  message: any;
  msg: Message;
  circle: string;
  type: string;
  reciever: string;

  @Input()
  messageObj: object;

  openDialog(): void {
    let dialogRef = this.dialog.open(AddMembersComponent, {
      width: '350px',
    });
  
  }

  ngOnChanges(value) {

    console.log('Object in msg', value.messageObj.currentValue.type);

    this.type = value.messageObj.currentValue.type;

    if (this.type === 'user') {

      this.reciever = value.messageObj.currentValue.value;

      this.messageService.getMessagesFromUser(this.reciever).subscribe (
        data => {
          this.messages = data.json();
        })
        console.log('this message',this.messages);
    } 
    else {
      this.circle=value.messageObj.currentValue.value;

      this.messageService.getMessagesFromCircle(value.messageObj.currentValue.value).subscribe(
        data => {
         
          this.messages = data.json();
        })
        
        console.log('this message',this.messages);
    }
  }

  onClick(msg: string, type: string) {
    console.log('type', type);
    console.log('message from text', msg);

    var request = { 'message': msg };

    this.msg = new Message();
    this.msg.message = msg;
    this.msg.recieverName = this.reciever;
    this.msg.senderName = localStorage.getItem('username');

    if (type === 'user') {
      this.messageService.sendMessageToUser(this.msg).subscribe(
        data => {
          if (data.status === 200) {
            let obj = {
              'senderName': localStorage.getItem('username'),
              'receiverId': this.reciever,
              'message': msg
            }
            this.messages.push(obj);
          }
        }
      )
    } else {
      this.messageService.sendMessageToCircle(this.msg).subscribe(
        data => {
          if (data.status === 200) {
            const obj = {
              'senderName': localStorage.getItem('username'),
              'receiverId': this.reciever,
              'message': msg
            }
            this.messages.push(obj);
          }
        })
    }
    this.message = "";
  }


  ngOnInit() {

  }
}
