import { Component, OnInit, Input,Output } from '@angular/core';
import { User } from './user';
import { UserService } from '../user.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


  
  users: User[];

  @Output() selectedUser = new EventEmitter<any>();

  constructor(private userService: UserService) { }

  getUsers() {
    
    this.userService.getUsers().subscribe(data => {
      this.users = data.json();
    })
  }


  selectUser(userData: string) {

    console.log('User Name:', userData);
    let currentUserName = {
      type:'user',
      value: userData
    }
    console.log('this user',currentUserName);
    this.selectedUser.emit(currentUserName);
  }


  ngOnInit() {
    this.getUsers();
  }
}
