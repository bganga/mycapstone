import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CircleService } from '../circle.service';
import { UserCircles } from './user-circles';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-user-circles',
  templateUrl: './user-circles.component.html',
  styleUrls: ['./user-circles.component.css']
})
export class UserCirclesComponent implements OnInit {

  constructor(private circleService: CircleService, private router: Router, public dialogRef: MatDialogRef<UserCirclesComponent>) { }

  createCircle(form: NgForm) {


    this.circleService.createCircle(form).subscribe(
      data => {
        if (data.status === 201) {
          this.circleService.addAdmin(form).subscribe(
            data => {
              if (data.status === 200) {
                  this.closeDialog();
                  this.router.navigate(['/dashboard']);
              }
            })
        }

      })
  }
  closeDialog() {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
