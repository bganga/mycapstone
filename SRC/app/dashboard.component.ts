import { Component, OnInit } from '@angular/core';
import {User} from '../user/user';
import { UserService } from '../user.service';
import {Circle} from '../circle/circle';
import { CircleService } from '../circle.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
heading="ChatUp";
User=localStorage.getItem('username');
  currentSelected: object;
  

  users: User[]=[];
  circles: Circle[]=[];
  // constructor(private userService: UserService,private circleService: CircleService) { }
constructor(private router: Router) { }
 
 onSelect(data){
  console.log(data);
  this.currentSelected=data;
  console.log('User in parent',this.currentSelected);

}

signout(){
  console.log("signout");
  localStorage.removeItem('token');
  localStorage.removeItem('username');
  localStorage.clear();
  this.router.navigate(['']);
}
 
//  getUsers() {
//    this.userService.getUsers().subscribe(data =>{
//      this.users=data.json();
//    })
//  }

//  getCircles() {
//   this.circleService.getCircles().subscribe(data =>{
//     this.circles=data.json();
//   })
// }


 ngOnInit(){
  //  this.getUsers();
  //  this.getCircles();
  
 }  

}
