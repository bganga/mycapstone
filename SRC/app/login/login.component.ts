import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
 export class LoginComponent implements OnInit {
 
  ngOnInit() { } 
  
  constructor(private userservice: UserService, private router: Router) { }

  logform(form: NgForm) {

    console.log(JSON.stringify(form.value));

    this.userservice.authenticateUser(form.value).subscribe(
      data => {
        if (data.status === 200)
        // tslint:disable-next-line:one-line
        {
         
          // location.reload();
          localStorage.setItem('token', data.json().token);
          localStorage.setItem('username', form.value.username);
          localStorage.setItem('password', form.value.password);
          this.router.navigate(['/dashboard']);

        }
      }
      // tslint:disable-next-line:semicolon
    )
  }
}
