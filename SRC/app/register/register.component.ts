import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    constructor(private userservice: UserService, private router: Router) { }
     
    registerUser(form:NgForm) {
    
    console.log(form.value);
    this.userservice.registerUser(form.value).subscribe(
      data => {
        if (data.status === 201) {
          this.router.navigate(['']);
        }
      }
    )
  }
  ngOnInit() {
  }
}
